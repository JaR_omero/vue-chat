import translator from '@/config/translator';

export default {
  install(Vue) {
    Vue.filter('translate', translator);
  },
};
