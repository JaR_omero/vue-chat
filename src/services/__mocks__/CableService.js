function CableService() {
  this.onDisconnect = null;
  this.onNewMessage = null;
  this.onNewUser = null;
  this.onUserDisconnect = null;

  return {
    connect: (
      username,
      handleConnection,
      handleErrors,
      onDisconnect,
    ) => {
      this.onDisconnect = onDisconnect;

      if (username.length < 3) {
        handleErrors(['Mock server error']);
      } else {
        handleConnection(username);
      }
    },

    connectToRoom: (
      roomId,
      onSubscriptionToRoom,
      onNewMessage,
      onNewUser,
      onUserDisconnect,
    ) => {
      onSubscriptionToRoom({ messages: [], users: [''] });
      this.onNewMessage = onNewMessage;
      this.onNewUser = onNewUser;
      this.onUserDisconnect = onUserDisconnect;
    },

    userReceived: (username) => {
      this.onNewUser({ username });
    },

    userDisconnectReceived: (username) => {
      this.onUserDisconnect({ username });
    },

    sendMessage: (text) => {
      this.onNewMessage({
        type: '',
        text,
      });
    },

    disconnect: () => {
      this.onDisconnect();
    },

    disconnectFromRoom: () => null,
  };
}

const instance = new CableService();

export default instance;
