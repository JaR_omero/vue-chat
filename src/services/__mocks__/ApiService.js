function ApiServiceMock() {
  return {
    getRooms: async page => new Promise((resolve) => {
      const response = page > 0
        ? {
          total: 31,
          rooms: [
            { id: 1, name: 'lorem' },
            { id: 2, name: 'lorem' },
            { id: 3, name: 'lorem' },
            { id: 4, name: 'lorem' },
            { id: 5, name: 'lorem' },
            { id: 6, name: 'lorem' },
            { id: 7, name: 'lorem' },
            { id: 8, name: 'lorem' },
            { id: 9, name: 'lorem' },
            { id: 10, name: 'lorem' },
          ],
        } : {
          errors: ['error'],
        };

      resolve(response);
    }),

    createRoom: async roomName => new Promise((resolve) => {
      if (roomName === 'error') {
        resolve({
          errors: ['error'],
        });
      } else {
        resolve({
          id: roomName,
          name: roomName,
        });
      }
    }),

    isMock: jest.fn(),
  };
}

const instance = new ApiServiceMock();

export default instance;
