import axios from 'axios';
import { API_SERVER } from '@/config/constants';
import i18n from '@/i18n';

const roomsPath = '/rooms';

function ApiService() {
  this.client = axios.create({
    baseURL: API_SERVER,
  });

  return {
    createRoom: async (roomName) => {
      try {
        const { data } = await this.client.post(
          roomsPath,
          { name: roomName },
          { validateStatus: status => [201, 400].includes(status) },
        );

        return data;
      } catch (e) {
        return { errors: [i18n.t('errors.unknown')] };
      }
    },
    getRooms: async (page) => {
      try {
        const { data } = await this.client.get(`${roomsPath}?page=${page}`);
        return data;
      } catch (e) {
        return { errors: [i18n.t('errors.unknown')] };
      }
    },

    // Raw client, for change config or modify adapter in tests
    httpClient: this.client,
  };
}

const instance = new ApiService();

export default instance;
