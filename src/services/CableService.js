import ActionCable from 'actioncable';
import { CABLE_SERVER } from '@/config/constants';
import i18n from '@/i18n';

const channelName = 'RoomChannel';

// Messages types from server side
/* eslint-disable no-unused-vars */
const TYPE_CONNECTED = 'connected';
const TYPE_ERROR = 'error';
const TYPE_SUBSCRIBE_ROOM = 'subscribe_room';
const TYPE_UNSUBSCRIBE_ROOM = 'unsubscribe_room';
const TYPE_CONNECTION = 'connection';
const TYPE_DISCONNECTION = 'disconnection';
const TYPE_NEW_MESSAGE = 'new_message';
/* eslint-enable no-unused-vars */

/**
 * Initialize ActionCable service and return the public interface of the service
 * @returns {{
 *  connect(),
 *  disconnect(),
 *  connectToRoom(),
 *  disconnectFromRoom(),
 * }}
 *
 * @constructor
 */
function CableService() {
  this.cable = null;
  this.channel = null;
  this.username = null;

  // Connection callbacks
  this.onSubscription = null;
  this.onSubscriptionError = null;

  // Connection timeout
  this.onSubscriptionTimeout = null;

  // Other callbacks
  this.onSubscriptionToRoom = null;
  this.onNewMessage = null;
  this.onNewUser = null;
  this.onUserDisconnect = null;
  this.onDisconnect = null;

  /**
   * Handle messages received from the server
   * @param message
   */
  this.received = (message) => {
    const { type, data } = message;
    switch (type) {
      // Clear timeout and send username to callback
      case TYPE_CONNECTED:
        clearTimeout(this.onSubscriptionTimeout);
        this.username = data.username;
        this.onSubscription(this.username);
        break;

      // Clear timeout and send errors to callback
      case TYPE_ERROR:
        clearTimeout(this.onSubscriptionTimeout);
        this.onSubscriptionError(data.errors);
        this.cable.disconnect();
        break;

      // Send room data to the callback
      case TYPE_SUBSCRIBE_ROOM:
        this.onSubscriptionToRoom(data);
        break;

      // New message received
      case TYPE_NEW_MESSAGE:
        this.onNewMessage(data);
        break;

      // New user connected to room
      case TYPE_CONNECTION:
        this.onNewUser(data);
        break;

      // Chat user disconnected
      case TYPE_DISCONNECTION:
        this.onUserDisconnect(data);
        break;

      // Fallback, unknown error, print response
      default:
        console.warn(message);
    }
  };

  /**
   * Return error message and disconnect the socket
   */
  this.onSubscriptionTimeout = () => {
    this.onSubscriptionError([i18n.t('errors.timeout')]);
    this.cable.disconnect();
  };

  /**
   * Action creator with expected format
   * @param type
   * @param data
   * @returns {{type: *, data: *}}
   */
  this.createAction = (type, data = null) => ({ type, data });

  /**
   * Create a message shape
   * @param text
   * @returns {{text: string, username: string, created_at: string, id: number}}
   */
  this.preSentMessage = text => ({
    text,
    username: this.username,
    created_at: new Date().toUTCString(),
    id: new Date().getTime().toString(),
    waiting: true,
  });

  /**
   * Return the public interface of the service
   */
  return {
    /**
     * Create a new websocket connection using ActionCable and binding callbacks
     * @param username
     * @param onSubscription
     * @param onSubscriptionError
     * @param onDisconnect
     */
    connect: (username, onSubscription, onSubscriptionError, onDisconnect) => {
      // Bind listeners
      this.onSubscription = onSubscription;
      this.onSubscriptionError = onSubscriptionError;
      this.onDisconnect = onDisconnect;

      // Disconnect previous cable if exist
      this.cable && this.cable.disconnect();

      // Create consumer with username param
      this.cable = ActionCable.createConsumer(`${CABLE_SERVER}?username=${username}`);

      // Connect and bind listeners
      this.channel = this.cable.subscriptions.create(channelName, {
        disconnected: onDisconnect,
        received: this.received,
      });

      // Run timeout error
      this.onSubscriptionTimeout = setTimeout(this.onSubscriptionTimeout, 3000);
    },

    /**
     * Disconnect from the server, automatically trigger onDisconnect callback
     */
    disconnect: () => {
      this.cable.disconnect();
      this.onDisconnect();
    },

    /**
     * Send request to the server for connect to a room by ID
     * @param roomId ID of the target room
     * @param onSubscriptionToRoom callback called when the server confirms the subscription
     * @param onNewMessage
     * @param onNewUser
     * @param onUserDisconnect
     */
    connectToRoom: (
      roomId,
      onSubscriptionToRoom,
      onNewMessage,
      onNewUser,
      onUserDisconnect,
    ) => {
      this.onSubscriptionToRoom = onSubscriptionToRoom;
      this.onNewMessage = onNewMessage;
      this.onNewUser = onNewUser;
      this.onUserDisconnect = onUserDisconnect;

      const payload = this.createAction(TYPE_SUBSCRIBE_ROOM, { room: roomId });
      this.channel.send(payload);
    },

    /**
     * Send request to the server for disconnect from the actual room subscription
     */
    disconnectFromRoom: () => {
      const payload = this.createAction(TYPE_UNSUBSCRIBE_ROOM);
      this.channel.send(payload);
    },

    sendMessage: (text) => {
      this.onNewMessage(this.preSentMessage(text));
      const payload = this.createAction(TYPE_NEW_MESSAGE, { text });
      this.channel.send(payload);
    },
  };
}

const instance = new CableService();

export default instance;
