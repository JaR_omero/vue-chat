import Vue from 'vue';
import Vuex from 'vuex';
import gui from '@/store/modules/gui';
import user from '@/store/modules/user';
import room from '@/store/modules/room';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    gui,
    user,
    room,
  },
});

export default store;
