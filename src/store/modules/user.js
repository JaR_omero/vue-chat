const SET_USERNAME = 'SET_USERNAME';

const initialState = {
  username: '',
};

const mutations = {
  [SET_USERNAME](state, username) {
    state.username = username;
  },
};

const actions = {};

const getters = {};

const module = {
  namespaced: true,
  state: initialState,
  mutations,
  actions,
  getters,
};

const mutationsTypes = {
  SET_USERNAME,
};

export {
  mutationsTypes,
  mutations,
  actions,
  getters,
};

export default module;
