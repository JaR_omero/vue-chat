// Mutations
import ApiService from '@/services/ApiService';

// Mutations room list
const SET_ROOMS_PAGE = 'SET_ROOMS_PAGE';
const SET_ERRORS = 'SET_ERRORS';
const SET_LOADING = 'SET_LOADING';

// Mutations room detail
const SET_ROOM = 'SET_ROOM';
const ADD_MESSAGE = 'ADD_MESSAGE';
const ADD_USER = 'ADD_USER';
const REMOVE_USER = 'REMOVE_USER';
const UPDATE_ROOM = 'UPDATE_ROOM';

// Actions
const FETCH_ROOMS_PAGE = 'FETCH_ROOMS_PAGE';

const initialState = {
  // Room list
  total: null,
  rooms: [],
  errors: [],
  loading: true,

  // Room detail
  room: {},
  loadingRoom: true,
};

const mutations = {
  [SET_ROOM](state, room) {
    state.room = room;
  },

  [UPDATE_ROOM](state, data) {
    state.room = {
      ...state.room,
      ...data,
    };
  },

  [ADD_MESSAGE](state, data) {
    // If message already exists with `waiting` state, replace it
    if (!data.waiting && state.room.messages) {
      // Filter waiting messages with same send text
      const msgIndex = state
        .room
        .messages
        .findIndex(msg => msg.waiting && msg.text === data.text);

      // Update messaged
      if (msgIndex !== -1) {
        state.room.messages[msgIndex] = {
          ...state.room.messages[msgIndex],
          ...data,
          waiting: false,
        };

        // Copy array to force re-render
        state.room.messages = [...state.room.messages];

        return;
      }
    }

    // Add the message to the room
    state.room.messages = [
      ...state.room.messages,
      data,
    ];
  },

  [ADD_USER](state, username) {
    state.room.users = [
      ...state.room.users,
      username,
    ];
  },

  [REMOVE_USER](state, username) {
    state.room.users = state.room.users.filter(user => user !== username);
  },

  [SET_ROOMS_PAGE](state, data) {
    state.rooms = data.rooms;
    state.total = data.total;
  },

  [SET_ERRORS](state, errors) {
    state.errors = errors;
  },

  [SET_LOADING](state, value) {
    state.loading = value;
  },
};

const actions = {
  async [FETCH_ROOMS_PAGE]({ commit }, page) {
    commit(SET_LOADING, true);
    commit(SET_ERRORS, []);

    const response = await ApiService.getRooms(page);

    if (response.errors) {
      commit(SET_ERRORS, response.errors);
    } else {
      commit(SET_ROOMS_PAGE, response);
    }

    commit(SET_LOADING, false);
  },
};

const getters = {};

const module = {
  namespaced: true,
  state: initialState,
  mutations,
  actions,
  getters,
};

const mutationsTypes = {
  SET_ROOMS_PAGE,
  SET_ROOM,
  ADD_MESSAGE,
  ADD_USER,
  REMOVE_USER,
  UPDATE_ROOM,
  SET_LOADING,
};

const actionsTypes = {
  FETCH_ROOMS_PAGE,
};

export {
  mutationsTypes,
  actionsTypes,
  mutations,
  actions,
  getters,
};

export default module;
