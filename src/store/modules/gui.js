// Mutation types
const TOGGLE_USERS_DIALOG = 'TOGGLE_USERS_DIALOG';
const TOGGLE_CREATE_ROOM_DIALOG = 'TOGGLE_CREATE_ROOM_DIALOG';
const OPEN_SNACKBAR = 'OPEN_SNACKBAR';
const SET_TITLE = 'SET_TITLE';

const initialState = {
  title: '',
  showUsersDialog: false,
  showRoomDialog: false,
  snackbar: {
    timeout: 3000,
    text: '',
    value: false,
  },
};

const mutations = {
  [SET_TITLE](state, title) {
    state.title = title;
  },

  [TOGGLE_USERS_DIALOG](state) {
    state.showUsersDialog = !state.showUsersDialog;
  },

  [TOGGLE_CREATE_ROOM_DIALOG](state) {
    state.showRoomDialog = !state.showRoomDialog;
  },

  /**
   * @param state
   * @param {object} options https://vuetifyjs.com/en/components/snackbars#api + text property for message
   */
  [OPEN_SNACKBAR](state, options) {
    state.snackbar = {
      ...state.snackbar,
      ...options,
    };
  },
};

const actions = {};

const getters = {};

// Module
const guiModule = {
  namespaced: true,
  state: initialState,
  mutations,
  actions,
  getters,
};

const mutationsTypes = {
  TOGGLE_USERS_DIALOG,
  TOGGLE_CREATE_ROOM_DIALOG,
  OPEN_SNACKBAR,
  SET_TITLE,
};

export {
  mutationsTypes,
  mutations,
  actions,
  getters,
};

export default guiModule;
