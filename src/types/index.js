const iconButton = {
  onClick: {
    type: Function,
  },
  label: {
    type: String,
  },
  icon: {
    type: String,
    default: 'send',
  },
  type: {
    type: String,
    default: 'button',
    validator: value => ['submit', 'button'].includes(value),
  },
  color: {
    type: String,
    default: 'normal',
  },
};

const room = {
  id: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  users: {
    type: Array,
    default: () => [],
  },
  messages: {
    type: Array,
    default: () => [],
  },
};

const users = {
  users: {
    type: Array,
    required: true,
  },
};

const user = {
  username: {
    type: String,
    required: true,
  },
};

const onSubmit = {
  onSubmit: {
    type: Function,
    required: true,
  },
};

const isLoading = {
  isLoading: {
    type: Boolean,
    required: true,
  },
};

const errors = {
  errors: {
    type: Array,
    required: true,
  },
};

export default {
  // common
  iconButton,
  onSubmit,
  isLoading,
  errors,

  // Rooms
  room,

  // Users
  users,
  user,
};
