const colors = [
  'red',
  'black',
  'blue',
  'green',
  'blueviolet',
  'darkorange',
  'deeppink',
  'navy',
  'goldenrod',
  'salmon',
  'sienna',
  'brown',
  'crimson',
  'darkblue',
  'darkgreen',
  'darkslategrey',
  'indigo',
  'indianred',
  'peru',
  'steelblue',
  'yellowgreen',
];

const randomColor = () => colors[Math.floor(Math.random() * colors.length)];

export {
  randomColor,
  colors,
};
