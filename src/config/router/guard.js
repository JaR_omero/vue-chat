import store from '@/store/store';
import { paths } from '@/config/router';

/**
 * Router middleware to check if the next route requires auth and
 * if the user is connected before entering
 * @param to
 * @param from
 * @param next
 */
export default function (to, from, next) {
  // `store.state ...` is better than `mapState('user', ['username']).username`
  const isAuth = !!store.state.user.username;
  const requiresAuth = to.matched.some(m => m.meta.requiresAuth);

  // If `to` requires auth and no auth provides, redirect to login
  if (requiresAuth && !isAuth) {
    next({ name: paths.HOME });
  } else {
    next();
  }
}
