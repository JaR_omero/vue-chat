import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/pages/LoginPage.vue';
import MainLayout from '@/layouts/MainLayout.vue';
import Room from '@/pages/RoomChatPage.vue';
import RoomList from '@/pages/RoomListPage.vue';
import NotFound from '@/pages/NotFoundPage.vue';
import RoomChatActions from '@/components/room/buttons/RoomChatActions.vue';
import RoomListActions from '@/components/room/buttons/RoomListActions.vue';
import loginGuard from '@/config/router/guard';

Vue.use(Router);

export const paths = {
  HOME: 'HOME',
  ROOM_INDEX: 'ROOM_INDEX',
  ROOM: 'ROOM',
};

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: paths.HOME,
      component: Login,
    },
    {
      component: MainLayout,
      path: '/salas',
      meta: {
        requiresAuth: true,
      },
      children: [
        {
          path: '',
          name: paths.ROOM_INDEX,
          components: {
            default: RoomList,
            button: RoomListActions,
          },
        },
        {
          path: ':id',
          name: paths.ROOM,
          components: {
            default: Room,
            button: RoomChatActions,
          },
        },
      ],
    },
    {
      path: '*',
      component: NotFound,
    },
  ],
});

router.beforeEach(loginGuard);

export default router;
