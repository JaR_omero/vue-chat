const { VUE_APP_SERVER } = process.env;

const CABLE_SERVER = `ws://${VUE_APP_SERVER}/cable`;
const API_SERVER = `http://${VUE_APP_SERVER}`;

const ITEMS_PER_PAGE = 10;

export {
  CABLE_SERVER,
  API_SERVER,
  ITEMS_PER_PAGE,
};
