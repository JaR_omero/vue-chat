const messages = {
  'Username already in use': 'El nombre de usuario ya está en uso',
  'Name is already taken': 'El nombre ya está en uso',
};

const absurdlySillyTranslator = text => messages[text] ? messages[text] : text;

export default absurdlySillyTranslator;
