import '@babel/polyfill';
import Vue from 'vue';
import '@/config/theme';
import App from '@/App.vue';
import router from '@/config/router';
import store from '@/store/store';
import i18n from '@/i18n';
import '@/components/globals';
import filters from '@/filters';

Vue.config.productionTip = process.env.NODE_ENV === 'production';

Vue.use(filters);

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
