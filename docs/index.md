# Análisis

## Librerías utilizadas

Además de Vue, se han instalado varios paquetes adicionales. La instalación del proyecto se ha realizado con ``@vue/cli``.

## Vuex

 Se ha usado para la gestión del estado de la aplicación. La store se ha dividido en tres módulos: GUI, usuarios y salas. En los proyectos que realizo con Redux me gusta dividir cada módulo en varios archivos siguiendo el patrón [re-ducks](https://github.com/alexnm/re-ducks) pero en este caso, debido a que los módulos eran muy reducidos y que era la primera vez que utilizaba Vuex he decidido mantener cada módulo en un solo archivo.

## Vuetify

Cuando no hay un diseño prestablecido suelo optar por alguna librería Material design, en este caso he encontrado esta entre las más populares para Vue y he decidido añadirla para probar qué tal aunque no fuera una dependencia imprescindible.

## Axios

Normalmente uso fetch para las peticiones a la API, pero llevaba tiempo queriendo probar esta librería así que he aprovechado para incluirla en el proyecto.

## Actioncable

Como era obvio debido al uso de ActionCable en el servidor, se usa esta librería para conectar más fácilmente mediante websockets.

## Jest

Como test runner y matchers he optado por Jest. Es la que uso en React actualmente y parece ser que es igualmente recomendada también en proyectos con Vue.

## Vue test utils

El testing unitario de los componentes se ayuda de esta librería.

## Sass [scss]

Suele ser mi elección de preprocesador en todos los proyectos.

## Otras librerías relevantes utilizadas

* Vue-router 
* Vue-i18n
