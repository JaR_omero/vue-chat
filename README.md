# vue-chat

### Analysis (spanish)

[Read analysis](docs/index.md)

## Run tests
```
yarn run test:unit
```

### Run tests with coverage
```
yarn run test:unit --coverage
```

## Project setup
```
yarn install
```

## Define environment variables

Copy ``.env.dist`` to ``.env`` and set ``VUE_APP_SERVER`` to actual [backend](https://bitbucket.org/JaR_omero/rails-chat) URL

Example ``.env``:

```
BASE_URL=/
VUE_APP_SERVER=localhost:3000
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```
