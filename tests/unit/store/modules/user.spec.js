import userModule, { mutations, mutationsTypes } from '@/store/modules/user';

describe('user store module', () => {
  describe('mutations', () => {
    let initialState;

    beforeEach(() => {
      initialState = { ...userModule.state };
    });

    it('usernmae', () => {
      mutations[mutationsTypes.SET_USERNAME](initialState, 'lorem');
      expect(initialState.username).toBe('lorem');
    });

  });
});
