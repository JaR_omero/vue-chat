import guiModule, { mutations, mutationsTypes } from '@/store/modules/gui';

describe('gui store module', () => {
  describe('mutations', () => {
    let initialState;

    beforeEach(() => {
      initialState = { ...guiModule.state };
    });

    it('mutation title', () => {
      mutations[mutationsTypes.SET_TITLE](initialState, 'new');
      expect(initialState.title).toBe('new');
    });

    it('mutation toggle users dialog', () => {
      toggleMutation(initialState, mutationsTypes.TOGGLE_USERS_DIALOG, 'showUsersDialog');
    });

    it('mutation toggle room create', () => {
      toggleMutation(initialState, mutationsTypes.TOGGLE_CREATE_ROOM_DIALOG, 'showRoomDialog');
    });

    it('mutation open snackbar', () => {
      mutations[mutationsTypes.OPEN_SNACKBAR](initialState, {
        hello: 'world',
      });

      expect(initialState.snackbar).toEqual({
        ...initialState.snackbar,
        hello: 'world',
      });
    });
  });
});

function toggleMutation(state, type, stateProperty) {
  mutations[type](state);
  expect(state[stateProperty]).toBe(true);

  mutations[type](state);
  expect(state[stateProperty]).toBe(false);
}
