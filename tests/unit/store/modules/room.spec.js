import roomModule, { mutations, actions, mutationsTypes, actionsTypes } from '@/store/modules/room';

describe('room store module', () => {
  describe('actions', () => {
    let commitFn;
    let initialState;

    beforeEach(() => {
      initialState = { ...roomModule.state };

      // Mock commit, run mutations against state
      commitFn = (type, payload) => mutations[type](initialState, payload);
    });

    it('fetch rooms page', async () => {
      await actions[actionsTypes.FETCH_ROOMS_PAGE]({ commit: commitFn }, 1);

      expect(initialState.loading).toBe(false);
      expect(initialState.errors).toEqual([]);
      expect(initialState.total).toBe(31); // See __mocks__/ApiService
      expect(initialState.rooms.length).toBe(10); // See __mocks__/ApiService
    });

    it('errors from fetch rooms page', async () => {
      await actions[actionsTypes.FETCH_ROOMS_PAGE]({ commit: commitFn }, -1);

      expect(initialState.loading).toBe(false);
      expect(initialState.errors.length).toEqual(1);
      expect(initialState.total).toBe(null);
      expect(initialState.rooms).toEqual([]);
    });
  });

  describe('mutations', () => {
    let initialState;

    beforeEach(() => {
      initialState = { ...roomModule.state };
    });

    it('mutation set room', () => {
      mutations[mutationsTypes.SET_ROOM](initialState, { id: 'l19' });
      expect(initialState.room).toEqual({ id: 'l19' });
    });

    it('mutation update room', () => {
      mutations[mutationsTypes.SET_ROOM](initialState, { id: 'l19' });
      mutations[mutationsTypes.UPDATE_ROOM](initialState, { name: 'lorem' });

      expect(initialState.room).toEqual({ id: 'l19', name: 'lorem' });
    });

    it('add new message', () => {
      // Initial room
      mutations[mutationsTypes.SET_ROOM](initialState, { messages: [] });

      mutations[mutationsTypes.ADD_MESSAGE](initialState, { text: 'ipsum' });
      expect(initialState.room.messages).toEqual([{ text: 'ipsum' }]);
    });

    it('update message', () => {
      // Initial room
      mutations[mutationsTypes.SET_ROOM](initialState, { messages: [] });

      // Add a new message
      mutations[mutationsTypes.ADD_MESSAGE](initialState, { text: 'ipsum', waiting: true });

      // Add a new message
      mutations[mutationsTypes.ADD_MESSAGE](initialState, { text: 'lorem' });

      // Already exists a waiting message like this, update message instead add
      mutations[mutationsTypes.ADD_MESSAGE](initialState, { text: 'ipsum' });

      // Three calls to ADD_MESSAGE, but only two added, the other was an update
      expect(initialState.room.messages.length).toBe(2);

      // Add a new message, waiting was updated before
      mutations[mutationsTypes.ADD_MESSAGE](initialState, { text: 'lorem' });

      expect(initialState.room.messages.length).toBe(3);
    });

    it('add new user', () => {
      // Initial room
      mutations[mutationsTypes.SET_ROOM](initialState, { users: ['Eleven'] });

      // Remove but not exist
      mutations[mutationsTypes.REMOVE_USER](initialState, 'Dustin');

      expect(initialState.room.users.length).toBe(1);

      mutations[mutationsTypes.REMOVE_USER](initialState, 'Eleven');

      // Successful remove
      expect(initialState.room.users.length).toBe(0);
    });

    it('set rooms page', () => {
      mutations[mutationsTypes.SET_ROOMS_PAGE](initialState, {
        rooms: [1, 9],
        total: 25,
      });

      expect(initialState.rooms).toEqual([1, 9]);
      expect(initialState.total).toBe(25);
    });

    it('loading', () => {
      expect(initialState.loading).toBe(true);

      mutations[mutationsTypes.SET_LOADING](initialState, false);

      expect(initialState.loading).toBe(false);

      mutations[mutationsTypes.SET_LOADING](initialState, true);

      expect(initialState.loading).toBe(true);
    });
  });
});
