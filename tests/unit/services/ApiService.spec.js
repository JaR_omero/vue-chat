import i18n from '@/i18n';

// unmock
const ApiService = require.requireActual('@/services/ApiService').default;

ApiService.httpClient.defaults.baseURL = '';

/**
 * Mock Http Adapter with fake responses
 * @param config
 * @returns {Promise<any>}
 */
ApiService.httpClient.defaults.adapter = config => new Promise((resolve, reject) => {
  const { method, url } = config;

  if (method === 'post' && url === '/rooms') {
    const roomName = JSON.parse(config.data).name;

    switch (roomName) {
      case 'reject':
        reject();
        break;
      default:
        resolve({
          data: { name: roomName },
          status: 201,
        });
    }
  } else if (method === 'get' && /\/rooms\?page=/.test(url)) {
    const page = parseInt(url[url.length - 1], 10);
    if (page) {
      resolve({
        data: 'something',
      });
    } else {
      reject();
    }
  }

  reject(new Error('Unknow action'));
});

describe('ApiService', () => {
  it('create room successful', async () => {
    const data = await ApiService.createRoom('hello');
    expect(data).toEqual({ name: 'hello' });
  });

  it('error on create', async () => {
    const data = await ApiService.createRoom('reject');
    expect(data).toEqual({ errors: [i18n.t('errors.unknown')] });
  });

  it('room list', async () => {
    const data = await ApiService.getRooms(2);
    expect(data).toEqual('something');
  });

  it('error on fetch room list', async () => {
    const data = await ApiService.getRooms();
    expect(data).toEqual({ errors: [i18n.t('errors.unknown')] });
  });
});
