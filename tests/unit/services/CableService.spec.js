import i18n from '@/i18n';

// Mock action cable library for use inside CableService
jest.mock('actioncable', function ActionCable() {
  this.disconnected;
  this.received;

  this.send = ({ type, data }) => {
    switch (type) {
      case 'subscribe_room':
        this.received({
          type: 'subscribe_room',
          data: { id: data.room },
        });
        break;
      case 'unsubscribe_room':
        // No callbacks, silence
        break;
      case 'new_message':
        this.received({
          type: 'new_message',
          data: 'bar',
        });
        break;
      default:
        throw new Error('Unhandled type on send');
    }
  };

  return {
    createConsumer: fullUrl => ({
      subscriptions: {
        create: (channelName, { disconnected, received }) => {
          this.disconnected = disconnected;
          this.received = received;

          // check username
          const url = require('url');
          const urlObject = new url.URL(fullUrl);
          const username = urlObject.searchParams.get('username');

          if (!username) {
            received({
              type: 'error',
              data: {
                errors: ['error'],
              },
            });

            return;
          }

          received({
            type: 'connected',
            data: {
              username: 'lorem', // not matter
            },
          });

          return {
            send: this.send,
          };
        },
      },
      disconnect: () => {
        this.disconnected();
      },
    }),
  };
});

// unmock
const CableService = require.requireActual('@/services/CableService').default;

describe('CableService', () => {
  let onSubscription,
    onSubscriptionError,
    onDisconnect,
    onSubscriptionToRoom,
    onNewMessage,
    onNewUser,
    onUserDisconnect;

  beforeEach(() => {
    // Set mocks callbacks
    onSubscription = jest.fn();
    onSubscriptionError = jest.fn();
    onDisconnect = jest.fn();
    onSubscriptionToRoom = jest.fn();
    onNewMessage = jest.fn();
    onNewUser = jest.fn();
    onUserDisconnect = jest.fn();
  });

  it('connect', async () => {
    CableService.connect('lorem', onSubscription, onSubscriptionError, onDisconnect);
    expect(onSubscription).toBeCalledWith('lorem');
  });

  it('connection error', async () => {
    CableService.connect('', onSubscription, onSubscriptionError, onDisconnect);
    expect(onSubscriptionError).toBeCalledWith(['error']);
  });

  it('disconnect', async () => {
    CableService.connect('lorem', onSubscription, onSubscriptionError, onDisconnect);
    CableService.disconnect();
    expect(onDisconnect).toBeCalledWith();
  });

  it('connect to room', async () => {
    CableService.connect('lorem', onSubscription, onSubscriptionError, onDisconnect);
    CableService.connectToRoom(19, onSubscriptionToRoom, onNewMessage, onNewUser, onUserDisconnect);
    expect(onSubscriptionToRoom).toBeCalledWith({ id: 19 });
  });

  it('disconnect from room', async () => {
    CableService.connect('lorem', onSubscription, onSubscriptionError, onDisconnect);

    // No callbacks, only check that no throw an error
    CableService.disconnectFromRoom();
  });

  it('send message', async () => {
    CableService.connect('lorem', onSubscription, onSubscriptionError, onDisconnect);
    CableService.connectToRoom(19, onSubscriptionToRoom, onNewMessage, onNewUser, onUserDisconnect);

    CableService.sendMessage('hello world');

    expect(onNewMessage.mock.calls.length).toBe(2);

    const firstCallFirstArgument = onNewMessage.mock.calls[0][0];
    expect(firstCallFirstArgument.text).toBe('hello world');
    expect(firstCallFirstArgument.waiting).toBe(true);

    expect(onNewMessage.mock.calls[1][0]).toEqual('bar');
  });
});
