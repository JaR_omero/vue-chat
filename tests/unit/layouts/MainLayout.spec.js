import { shallowMount } from '@vue/test-utils';
import MainLayout from '@/layouts/MainLayout.vue';
import stubs from '#/config/stubsList';

describe('MainLayout.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(MainLayout, {
      stubs,
      propsData: {
        placeholder: '',
        value: '',
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
