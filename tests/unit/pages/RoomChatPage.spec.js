import { shallowMount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import RoomChatPage from '@/pages/RoomChatPage.vue';
import store from '@/store/store';
import i18n from '@/i18n';
import stubs from '#/config/stubsList';
import CableService from '@/services/CableService';

const localVue = createLocalVue();

describe('RoomChatPage.vue', () => {
  let wrapper;

  beforeEach(() => {
    // Set initial room
    store.commit('room/SET_ROOM', {
      id: 'l19',
      name: 'lorem',
    });

    wrapper = shallowMount(RoomChatPage, {
      localVue,
      store,
      i18n,
      router: new Router(),
      stubs,
    });

    // Mock refs and properties
    wrapper.vm.$refs = {
      layout: { $refs: { messages: { scrollTop: 0, scrollHeight: 0 } } },
    };
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('Testing connection with ActionCable', () => {
    const { vm } = wrapper;

    expect(vm.room.messages.length).toBe(0);

    vm.onSend('hello world');

    // Callback to add messages works
    expect(vm.room.messages.length).toBe(1);

    // Force connection
    CableService.userReceived('Luka');

    // Callback to add users works
    expect(vm.room.users.length).toBe(2);

    // Force connection
    CableService.userDisconnectReceived('Luka');

    // Callback to remove users works
    expect(vm.room.users.length).toBe(1);
  });
});
