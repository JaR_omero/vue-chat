import { shallowMount, createLocalVue } from '@vue/test-utils';
import NotFoundPage from '@/pages/NotFoundPage.vue';
import i18n from '@/i18n';
import stubsList from '#/config/stubsList';

const localVue = createLocalVue();

describe('NotFoundPage.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(NotFoundPage, {
      localVue,
      i18n,
      stubs: stubsList,
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
