import { shallowMount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import LoginPage from '@/pages/LoginPage.vue';
import CableService from '@/services/CableService';

const localVue = createLocalVue();

describe('LoginPage.vue', () => {
  let router;

  beforeEach(() => {
    router = new VueRouter();
  });

  it('renders correctly', () => {
    const wrapper = shallowMount(LoginPage);
    expect(wrapper).toMatchSnapshot();
  });

  it('flow with CableService', () => {
    const wrapper = shallowMount(LoginPage, { localVue, router });
    const { vm } = wrapper;

    // Mock functions with depends on vuex, router and i18n
    vm.setUsername = jest.fn();
    vm.openSnackbar = jest.fn();
    vm.$router.push = jest.fn();

    // Test initial state
    expect(vm.isLoading).toBe(false);
    expect(vm.usernameErrors.length).toBe(0);

    // Call with invalid username
    vm.onSubmit('');

    // False because synchronous call callbacks in CableService mock
    expect(vm.isLoading).toBe(false);

    // Expect errors retrieve from server
    expect(vm.usernameErrors.length).toEqual(1);

    // Call with valid username
    const username = 'Luka';
    vm.onSubmit(username);

    expect(vm.isLoading).toBe(false);
    expect(vm.usernameErrors.length).toEqual(0);
    expect(vm.setUsername.mock.calls.length).toBe(1);
    expect(vm.setUsername).toBeCalledWith(username);

    // Disconnect
    CableService.disconnect();

    expect(vm.setUsername.mock.calls.length).toBe(2);
    expect(vm.setUsername).toBeCalledWith('');
  });
});
