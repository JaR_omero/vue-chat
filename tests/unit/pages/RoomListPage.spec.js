import { shallowMount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import RoomListPage from '@/pages/RoomListPage.vue';
import store from '@/store/store';
import i18n from '@/i18n';
import { paths } from '@/config/router';
import stubs from '#/config/stubsList';

const localVue = createLocalVue();

describe('RoomListPage.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomListPage, {
      localVue,
      store,
      i18n,
      router: new Router(),
      stubs,
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('test pagination', async () => {
    const { vm } = wrapper;

    expect(vm.rooms.length).toBe(10);
    expect(vm.pages).toBe(4);

    await vm.handlePagination(5);

    expect(vm.page).toBe(5);
  });

  it('go to room', async () => {
    const { vm } = wrapper;

    // Open dialog and check it works before and after
    expect(vm.showRoomDialog).toBe(false);
    vm.toggleDialog();
    expect(vm.showRoomDialog).toBe(true);

    // mock methods
    vm.setRoom = jest.fn();
    vm.$router.push = jest.fn();

    vm.goToRoom({ id: 19 });

    // close dialog
    expect(vm.showRoomDialog).toBe(false);
    expect(vm.setRoom).toBeCalledWith({ id: 19 });
    expect(vm.$router.push).toBeCalledWith({ name: paths.ROOM, params: { id: 19 } });
  });
});
