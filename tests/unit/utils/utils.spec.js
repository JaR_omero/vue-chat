import { colors, randomColor } from '@/utils/utils';

describe('utils', () => {
  it('random color', () => {
    const iterations = colors.length * 10; // try to cover all posibilities
    for (let i = 0; i < iterations; i++) {
      // More than colors, check mainly that it does not return a random value out of range
      expect(colors).toContain(randomColor());
    }
  });
});
