import { shallowMount, createLocalVue } from '@vue/test-utils';
import LoginForm from '@/components/login/LoginForm.vue';
import i18n from '@/i18n';
import stubsList from '#/config/stubsList';

const localVue = createLocalVue();
localVue.filter('translate', () => '');

describe('LoginForm.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(LoginForm, {
      localVue,
      i18n,
      stubs: stubsList,
      propsData: {
        onSubmit: jest.fn(),
        errors: [],
        isLoading: false,
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('handle submit', () => {
    const { vm } = wrapper;

    vm.handleSubmit();

    // Check errors
    expect(vm.formErrors.length).toBe(2);
    expect(wrapper.contains('ul')).toBe(true);

    vm.username = 'aaaaaaaaaaaaaaaa';
    vm.handleSubmit();

    // Check errors
    expect(vm.formErrors.length).toBe(1);

    // Not called
    expect(vm.onSubmit.mock.calls.length).toBe(0);

    vm.username = 'Light';
    vm.handleSubmit();

    // Submit correctly
    expect(vm.formErrors.length).toBe(0);
    expect(vm.onSubmit).toBeCalledWith('Light');
    expect(wrapper.contains('ul')).toBe(false);
  });
});
