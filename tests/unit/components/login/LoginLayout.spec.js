import { shallowMount } from '@vue/test-utils';
import LoginLayout from '@/components/login/LoginLayout.vue';
import stubs from '#/config/stubsList';

describe('LoginLayout.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(LoginLayout, {
      stubs,
      propsData: {
        onSubmit: jest.fn(),
        errors: [],
        isLoading: false,
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
