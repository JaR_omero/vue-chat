import { shallowMount } from '@vue/test-utils';
import RoomCreate from '@/components/room/create/RoomCreate.vue';
import stubs from '#/config/stubsList';
import i18n from '@/i18n';

describe('RoomCreate.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomCreate, {
      stubs,
      i18n,
      propsData: {
        setRoom: jest.fn(),
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('onSubmit', async () => {
    const { vm } = wrapper;

    // two errors on validation
    await vm.checkForm();

    expect(vm.errors.length).toBe(2);
    expect(vm.loading).toBe(false);

    // error because name is too long
    vm.roomName = 'aaaaaaaaaaaaaaaaaaaaaaaaaa';
    await vm.checkForm();

    expect(vm.errors.length).toBe(1);
    expect(vm.loading).toBe(false);

    // Never call setRoom if errors
    expect(vm.setRoom.mock.calls.length).toBe(0);

    // force return error from ApiService mock
    vm.roomName = 'error';
    await vm.checkForm();
    expect(vm.errors.length).toBe(1);

    // Valid submit
    const roomName = 'Imladris';
    vm.roomName = roomName;
    await vm.checkForm();

    expect(vm.errors.length).toBe(0);
    expect(vm.loading).toBe(false);

    expect(vm.setRoom).toBeCalledWith({ id: roomName, name: roomName });
  });
});
