import { shallowMount } from '@vue/test-utils';
import MessagesList from '@/components/room/messages/MessagesList.vue';
import stubs from '#/config/stubsList';

describe('MessagesList.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(MessagesList, {
      stubs,
      propsData: {
        messages: [{
          id: '1',
          text: 'Hello',
          username: 'Mike',
          created_at: '',
        }],
        myself: '',
        colors: { Mike: 'red' },
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('get color', () => {
    expect(
      wrapper.find('messageslistitem-stub').props().color
    ).toBe('red');
  });
});
