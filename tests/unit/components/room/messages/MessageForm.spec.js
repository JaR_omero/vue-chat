import { shallowMount } from '@vue/test-utils';
import MessageForm from '@/components/room/messages/MessageForm.vue';
import stubs from '#/config/stubsList';

describe('MessageForm.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(MessageForm, {
      stubs,
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('submit message form', () => {
    const { vm } = wrapper;

    vm.onSend();

    expect(wrapper.emitted().send).toBeFalsy();

    vm.message = '     hello';
    vm.onSend();

    expect(wrapper.emitted().send[0]).toEqual(['hello']);
    expect(vm.message).toBe('');
  });
});
