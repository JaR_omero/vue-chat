import { shallowMount } from '@vue/test-utils';
import MessagesListItem from '@/components/room/messages/MessagesListItem.vue';
import stubs from '#/config/stubsList';

describe('MessagesListItem.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(MessagesListItem, {
      stubs,
      propsData: {
        id: '1',
        text: 'Hello',
        username: 'Mike',
        created_at: '2018-09-05T17:50:11.965Z',
        owner: true,
        waiting: true,
        color: 'red',
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('timestamp', () => {
    const { vm } = wrapper;
    const date = new Date(vm.created_at);
    const hours = date.getHours();

    expect(wrapper.vm.time).toBe(`${hours}:50`);
    expect(wrapper.vm.fullDate).toBe(`${hours}:50 - 5/9/2018`);
  });
});
