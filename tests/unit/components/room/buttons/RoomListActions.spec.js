import { shallowMount } from '@vue/test-utils';
import RoomListActions from '@/components/room/buttons/RoomListActions.vue';
import i18n from '@/i18n';
import stubs from '#/config/stubsList';

describe('RoomListActions.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomListActions, {
      stubs,
      i18n,
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
