import { shallowMount } from '@vue/test-utils';
import RoomChatActions from '@/components/room/buttons/RoomChatActions.vue';
import i18n from '@/i18n';
import stubs from '#/config/stubsList';
import router from '@/config/router';

describe('RoomChatActions.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomChatActions, {
      stubs,
      i18n,
      router,
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('onClickBack callback not throw', () => {
    wrapper.vm.onClickBack();
  });
});
