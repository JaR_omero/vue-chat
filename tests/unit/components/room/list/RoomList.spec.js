import { shallowMount } from '@vue/test-utils';
import RoomList from '@/components/room/list/RoomList.vue';
import stubs from '#/config/stubsList';

describe('RoomList.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomList, {
      stubs,
      propsData: {
        rooms: [{ id: 'id1', name: 'name1' }],
        onClickRoom: jest.fn(),
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
