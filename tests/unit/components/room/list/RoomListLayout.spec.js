import { shallowMount } from '@vue/test-utils';
import RoomListLayout from '@/components/room/list/RoomListLayout.vue';
import i18n from '@/i18n';
import stubs from '#/config/stubsList';

describe('RoomListLayout.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomListLayout, {
      i18n,
      stubs,
      propsData: {
        rooms: [],
        loading: false,
        errors: [],
        actualPage: 1,
        pages: 10,
        onClickRoom: jest.fn(),
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
