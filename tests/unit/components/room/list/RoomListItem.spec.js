import { shallowMount } from '@vue/test-utils';
import RoomListItem from '@/components/room/list/RoomListItem.vue';
import stubs from '#/config/stubsList';

describe('RoomListItem.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomListItem, {
      stubs,
      propsData: {
        id: 'id1',
        name: 'name1',
        onClickRoom: jest.fn(),
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('onSubmit', () => {
    wrapper.vm.onClick();
    expect(wrapper.vm.onClickRoom).toBeCalledWith({ id: 'id1', name: 'name1' });
  });
});
