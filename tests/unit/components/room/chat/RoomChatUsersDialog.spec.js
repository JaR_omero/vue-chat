import { shallowMount } from '@vue/test-utils';
import RoomChatUsersDialog from '@/components/room/chat/RoomChatUsersDialog.vue';
import store from '@/store/store';
import i18n from '@/i18n';
import stubs from '#/config/stubsList';

describe('RoomChatUsersDialog.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomChatUsersDialog, {
      stubs,
      store,
      i18n,
      propsData: {
        users: [{}, {}],
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
