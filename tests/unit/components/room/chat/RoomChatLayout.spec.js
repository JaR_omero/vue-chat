import { shallowMount } from '@vue/test-utils';
import RoomChatLayout from '@/components/room/chat/RoomChatLayout.vue';
import store from '@/store/store';
import stubs from '#/config/stubsList';
import { colors as listOfColors } from '@/utils/utils';

describe('RoomChatLayout.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomChatLayout, {
      stubs,
      store,
      propsData: {
        myself: '',
        id: '',
        name: '',
        messages: [
          { username: 'Kira' },
          { username: 'Ryuk' },
          { username: 'Yagami' },
          { username: 'L' },
        ],
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('random color for each user', async () => {
    const { vm: { colors, messages } } = wrapper;

    const usernames = messages.map(msg => msg.username);

    expect(messages.length).toBe(4);
    expect(Object.keys(colors).length).toBe(4);

    // Check colors shape
    for (const username of Object.keys(colors)) {
      expect(listOfColors).toContain(colors[username]);
      expect(usernames).toContain(username);
    }
  });
});
