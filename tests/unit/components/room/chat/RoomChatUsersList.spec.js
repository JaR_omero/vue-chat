import { shallowMount } from '@vue/test-utils';
import RoomChatUsersList from '@/components/room/chat/RoomChatUsersList.vue';
import stubs from '#/config/stubsList';

describe('RoomChatUsersList.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomChatUsersList, {
      stubs,
      propsData: {
        username: '',
        users: ['us1', 'us2'],
        self: 'us2',
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
