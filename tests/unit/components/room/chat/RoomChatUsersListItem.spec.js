import { shallowMount } from '@vue/test-utils';
import RoomChatUsersListItem from '@/components/room/chat/RoomChatUsersListItem.vue';
import stubs from '#/config/stubsList';

describe('RoomChatUsersListItem.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(RoomChatUsersListItem, {
      stubs,
      propsData: {
        isSelf: false,
        username: 'test',
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
