import { shallowMount } from '@vue/test-utils';
import AppLogo from '@/components/common/AppLogo.vue';

describe('AppLogo.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AppLogo);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
