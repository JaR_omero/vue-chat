import { shallowMount } from '@vue/test-utils';
import AppTitle from '@/components/common/AppTitle.vue';
import store from '@/store/store';

describe('AppTitle.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AppTitle, {
      store,
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
