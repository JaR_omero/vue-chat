import { shallowMount } from '@vue/test-utils';
import AppHeader from '@/components/common/AppHeader.vue';
import stubs from '#/config/stubsList';

describe('AppHeader.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AppHeader, {
      stubs,
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
