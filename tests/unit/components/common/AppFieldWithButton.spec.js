import { shallowMount } from '@vue/test-utils';
import AppFieldWithButton from '@/components/common/AppFieldWithButton.vue';
import stubs from '#/config/stubsList';

describe('AppFieldWithButton.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AppFieldWithButton, {
      stubs,
      propsData: {
        placeholder: '',
        value: '',
      },
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
