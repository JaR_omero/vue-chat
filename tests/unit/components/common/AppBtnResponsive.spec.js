import { shallowMount } from '@vue/test-utils';
import AppBtnResponsive from '@/components/common/AppBtnResponsive.vue';
import stubs from '#/config/stubsList';

describe('AppBtnResponsive.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AppBtnResponsive, {
      stubs,
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
