import { shallowMount } from '@vue/test-utils';
import AppSnackbar from '@/components/common/AppSnackbar.vue';
import stubs from '#/config/stubsList';
import store from '@/store/store';

describe('AppSnackbar.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AppSnackbar, {
      stubs,
      store,
    });
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
