export default [
  'v-btn',
  'v-icon',
  'v-dialog',
  'v-list',
  'v-list-tile',
  'v-list-tile-title',
  'v-list-tile-content',
  'v-list-tile-avatar',
  'v-alert',
  'v-pagination',
  'v-textarea',
  'v-snackbar',
  'v-card',
  'v-card-text',
  'v-card-actions',
  'v-card-title',
  'v-divider',
  'v-spacer',
  'v-text-field',
  'app-field-with-button',
  'app-logo',
  'app-title',
  'app-btn-responsive',
  'app-header',
  'router-view',
];
